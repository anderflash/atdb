#ifndef AT_I_BANCO_H
#define AT_I_BANCO_H

#include <glib-object.h>
#include "atiregistro.h"

G_BEGIN_DECLS

#define AT_TYPE_I_BANCO at_i_banco_get_type()
G_DECLARE_INTERFACE(AtIBanco, at_i_banco, AT, I_BANCO, GObject)
;
struct _AtIBancoInterface
{
    GTypeInterface g_iface;
    void          (* conectar)    (AtIBanco* banco, GError** erro);
    void          (* fechar)      (AtIBanco* banco, GError** erro);
    void          (* adicionar)   (AtIBanco* banco, AtIRegistro* registro, GError** erro);
    void          (* remover)     (AtIBanco* banco, AtIRegistro* registro, GError** erro);
    void          (* atualizar)   (AtIBanco* banco, AtIRegistro* registro, GError** erro);
    AtIRegistro*  (* procurar)    (AtIBanco* banco, AtIRegistro* criterio);
    AtITabela*    (* get_tabelas) (AtIBanco* banco);
    void          (* set_tabelas) (AtIBanco* banco, AtITabela* tabelas);
    void          (* metadados2Tabelas) (AtIBanco* banco);
    void          (* tabelas2Metadados) (AtIBanco* banco);
};

/**
 * @brief Conectar-se a um banco de dados
 * 
 * @param banco ...
 * @param erro ...
 * @return void
 */
void         at_i_banco_conectar         (AtIBanco* banco, GError** erro);
void         at_i_banco_fechar           (AtIBanco* banco, GError** erro);
void         at_i_banco_adicionar        (AtIBanco* banco, AtIRegistro* registro, GError** erro);
void         at_i_banco_remover          (AtIBanco* banco, AtIRegistro* registro, GError** erro);
void         at_i_banco_atualizar        (AtIBanco* banco, AtIRegistro* registro, GError** erro);
AtIRegistro* at_i_banco_procurar         (AtIBanco* banco, AtIRegistro* criterio);
AtITabela*   at_i_banco_get_tabelas      (AtIBanco* banco);
void         at_i_banco_set_tabelas      (AtIBanco* banco, AtITabela* tabelas);
void         at_i_banco_metadados2Tabelas(AtIBanco* banco, GError** erro);
void         at_i_banco_tabelas2Metadados(AtIBanco* banco, GError** erro);

#define AT_BANCO_ERROR at_banco_error_quark()
enum
{
    AT_BANCO_ERROR_CONECTAR,
    AT_BANCO_ERROR_FECHAR,
    AT_BANCO_ERROR_ADICIONAR,
    AT_BANCO_ERROR_REMOVER,
    AT_BANCO_ERROR_ATUALIZAR
};

G_END_DECLS
#endif
