#include <glib.h>
#include <glib/gi18n.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <

#define GETTEXT_PACKAGE "atdbconverter"
#define DATADIR "/usr/share"

static gchar* tipoOrigem  = 0;
static gchar* tipoDestino = 0;
static gchar* entrada     = NULL;
static gint   porta       = 0;
static gchar* saida       = NULL;

static GOptionEntry entries[] = 
{
  {"formato-origem",  'o', 0, G_OPTION_ARG_STRING, &tipoOrigem, "formato de origem","N"},
  {"formato-destino", 'd', 0, G_OPTION_ARG_STRING, &tipoDestino,"formato de destino", "M"},
  {"entrada",         'd', 0, G_OPTION_ARG_STRING, &entrada, "entrada: arquivo ou banco de dados.","N"},
  {"porta",           'p', 0, G_OPTION_ARG_INT, &porta, "porta para se conectar ao servidor","M"},
  {"saida",           's', 0, G_OPTION_ARG_STRING,&saida,"saída: arquivo ou banco de dados","N"}
};

void convJson2Mysql(entrada, saida, porta){
  g_json  
}
void convJson2Postgre(entrada, saida, porta){
    
}
void convJson2GObject(entrada, saida, porta){
    
}
void convMysql2Json(entrada, saida, porta){
    
}
void convMysql2Postgre(entrada, saida, porta){
    
}
void convMysql2Gobject(entrada, saida, porta){
    
}
void convPostgre2Json(entrada, saida, porta){
    
}
void convPostgre2Mysql(entrada, saida, porta){
    
}
void convPostgre2Gobject(entrada, saida, porta){
    
}
void convGobject2Json(entrada, saida, porta){
    
}
void convGobject2Mysql(entrada, saida, porta){
    
}
void convGobject2Postgre(entrada, saida, porta){
    
}

int main(int argc, char** argv)
{
  setlocale(LC_ALL, "");
  bindtextdomain(GETTEXT_PACKAGE, DATADIR "/locale");
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
  textdomain(GETTEXT_PACKAGE);
  GError *error = NULL;
  GOptionContext *context;
  
  entries[0].arg_description = _("formato de origem");
  entries[1].arg_description = _("formato de destino");
  entries[2].arg_description = _("entrada");
  entries[3].arg_description = _("saida");
  entries[4].arg_description = _("porta");

  context = g_option_context_new("- test tree model performance");
  g_option_context_add_main_entries(context, entries, GETTEXT_PACKAGE);
  //g_option_context_add_group(context, );
  if(!g_option_context_parse(context, &argc, &argv, &error))
  {
    g_print(_("option parsing failed: %s\n"), error->message);
    exit(1);
  }
  gint jsonOrigem     = g_ascii_strcasecmp(tipoOrigem,  "json");
  gint jsonDestino    = g_ascii_strcasecmp(tipoDestino, "json");
  gint mysqlOrigem    = g_ascii_strcasecmp(tipoOrigem,  "mysql");
  gint mysqlDestino   = g_ascii_strcasecmp(tipoDestino, "mysql");
  gint postgreOrigem  = g_ascii_strcasecmp(tipoOrigem,  "postgre");
  gint postgreDestino = g_ascii_strcasecmp(tipoDestino, "postgre");
  gint gobjectOrigem  = g_ascii_strcasecmp(tipoOrigem,  "gobject");
  gint gobjectDestino = g_ascii_strcasecmp(tipoDestino, "gobject");
  
  
  if     (jsonOrigem    | mysqlDestino   == 0) convJson2Mysql     (entrada, saida, porta);
  else if(jsonOrigem    | postgreDestino == 0) convJson2Postgre   (entrada, saida, porta);
  else if(jsonOrigem    | gobjectDestino == 0) convJson2GObject   (entrada, saida, porta);
  else if(mysqlOrigem   | jsonDestino    == 0) convMysql2Json     (entrada, saida, porta);
  else if(mysqlOrigem   | postgreDestino == 0) convMysql2Postgre  (entrada, saida, porta);
  else if(mysqlOrigem   | gobjectDestino == 0) convMysql2Gobject  (entrada, saida, porta);
  else if(postgreOrigem | jsonDestino    == 0) convPostgre2Json   (entrada, saida, porta);
  else if(postgreOrigem | mysqlDestino   == 0) convPostgre2Mysql  (entrada, saida, porta);
  else if(postgreOrigem | gobjectDestino == 0) convPostgre2Gobject(entrada, saida, porta);
  else if(gobjectOrigem | jsonDestino    == 0) convGobject2Json   (entrada, saida, porta);
  else if(gobjectOrigem | mysqlDestino   == 0) convGobject2Mysql  (entrada, saida, porta);
  else if(gobjectOrigem | postgreDestino == 0) convGobject2Postgre(entrada, saida, porta);
  
  
  
  printf("%d\n", tipoOrigem);

  return 0;
}
