#include "atibanco.h"

G_DEFINE_INTERFACE(AtIBanco, at_i_banco, G_TYPE_OBJECT);
static void
at_i_banco_default_init(AtIBancoInterface *iface){
    
}

void at_i_banco_conectar(AtIBanco* banco, GError** erro){
    AT_I_BANCO_GET_IFACE (banco)->conectar(banco,erro);
}
void at_i_banco_fechar(AtIBanco* banco, GError** erro){
    AT_I_BANCO_GET_IFACE (banco)->fechar(banco,erro);
}
void at_i_banco_adicionar(AtIBanco* banco, AtIRegistro* registro, GError** erro){
    AT_I_BANCO_GET_IFACE (banco)->adicionar(banco,registro,erro);
}
void at_i_banco_remover(AtIBanco* banco, AtIRegistro* registro, GError** erro){
    AT_I_BANCO_GET_IFACE (banco)->remover(banco,registro,erro);
}
void at_i_banco_atualizar(AtIBanco* banco, AtIRegistro* registro, GError** erro){
    AT_I_BANCO_GET_IFACE (banco)->atualizar(banco,registro,erro);
}
AtIRegistro* at_i_banco_procurar(AtIBanco* banco, AtIRegistro* criterio){
    return AT_I_BANCO_GET_IFACE (banco)->procurar(banco,criterio);
}

G_DEFINE_QUARK(at_banco_error_quark, at_banco_error)